#!/bin/bash

root="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

function get-status() {
    if [[ $1 -eq 0 ]]; then
      echo "\e[32mSUCCESS\e[0m";
    else
      echo "\e[31mFAILED\e[0m";
    fi
}

pushd "$root" >/dev/null

echo -e "=========== YAML Lint ===========\n"
yamllint .
rc1=$?
status=$(get-status $rc1)
echo -e "=== YAML Lint status: $status ===\n"

echo -e "=========== Ansible Lint ===========\n"
ansible-lint roles/*/
rc2=$?
status=$(get-status $rc2)
echo -e "=== Ansible Lint Status: $status ===\n"

popd >/dev/null

exit $((rc1 + rc2))
