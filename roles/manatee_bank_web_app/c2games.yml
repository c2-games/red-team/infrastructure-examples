---
Title: 'Manatee Bank Web App'
Version: 1.0
Author: 'Bryer Esengard'
Organization: 'C2 Games'
Type: 'Service'
Difficulty: 7

Description: >
  Installs Apache web server, PHP modules clones a vulnerable web service from
  a public repository into the default web directory, `/var/www/html`.
  The Manatee Bank vulnerable web application is vulnerable to SQL Injection,
  Command Injection, and requires a MySQL backend.

OSCompatibility:
  - Family: Linux
    Name: Ubuntu
    Version: 18.04
  - Family: Linux
    Name: CentOS
    Version: 7

Exploitation:
  - Example: curl -XPOST --cookie-jar ./curl-cookies.txt -d "username=user" -d "password=' OR '1'='1" "${URL}/login.php"
    Description: >
      Authenticate as any user via SQL Injection. Can be performed manually by navigating to login page in browser
      and entering `' OR '1'='1` in the password box.
  - Example: curl -XPOST --stderr - --cookie ./curl-cookies.txt -d "cmd=ls" "http://$HOST/advanced.php"
    Description: >
      Command Injection into `cmd` page parameter. This exploit can be triggered using cURL, or
      by navigating to $HOST/advanced.php in a browser and using the UI.


# Mitigation: This is a short description of how a student would be able to
#   mitigate the misconfiguration or exploit, or secure the vulnerable service.

Mitigation: >
  The Command injection vulnerability can be mitigated by removing the advanced.php
  page from `/var/www/html`, changing the file extension of advanced.php, or disabling
  the variable "debug" in the configuration file. Multiple hints are provided within the
  PHP file and the configuration file.
  .
  The MySQL Inject vulnerability can only be mitigated by modifying the PHP code, and
  is considered a very difficult challenge to mitigate.

# Training: This is a short description of how students might be trained
#   on the skills required for this Challenge. This shouldn't give away the
#   exact challenge, but should incorporate the skills required to hunt for
#   the vulnerability.

Training: >
  Students should be aware of the services that are running on their systems,
  and common security issues with these types of applications.
  By familiarizing themselves with the service and the organization requirements,
  they can make reasonable decisions to disable unused features of applications
  that may be causing their infrastructure to be less secure.

# Justification: The justification this challenge should be included
#   as an Infrastructure Challenge. This documents the key educational
#   elements or topics that are covered or reinforced by the challenge.
#   It may also include specific skills that are required to be able
#   to solve the challenge. This will be used to evaluate student competencies.

Justification: >
  This challenge covers the skills required to quickly analyze and evaluate
  a service for potential vulnerabilities, and patch vulnerabilities when
  vendor support is not available. The student must demonstrate that they
  can identify invalid or insecure configurations within an existing
  environment, and reconfigure the service to a functional and secure state.
