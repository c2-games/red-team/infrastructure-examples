# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
    #
    # Global configurations across all VMs
    #

    # Add IP address to TTY prompt
    config.vm.provision "shell", inline: "echo -e '\nIP Address: \\4{eth1}\n' >> /etc/issue"

    # Copy in Ansible SSH key and set up under vagrant user
    config.vm.provision "file", source: ".ssh/", destination: ".ssh"
    config.vm.provision "shell", privileged: false, inline: "cat ~/.ssh/ansible_key.pub >> ~/.ssh/authorized_keys"
    config.vm.provision "shell", privileged: false, inline: "chmod 600 ~/.ssh/authorized_keys ~/.ssh/config"
    config.vm.provision "shell", privileged: false, inline: "chmod 600 ~/.ssh/ansible_key*"

    # Create a second network adapter, and put VM on public network (bridged network)
    # The first adapter is a special internal adapter used by vagrant
    config.vm.network "public_network"

    # Enable password-less sudo for everyone - VERY INSECURE, for test/dev machines only!
    config.vm.provision "shell", inline: "sudo echo 'ALL ALL = (ALL) NOPASSWD: ALL' >> /etc/sudoers.d/c2games"

    #
    # Separate VM definitions and System specific configurations
    #
    config.vm.define "dev", primary: true do |dev|
        dev.vm.box = "generic/ubuntu1804"
        dev.vm.hostname = "dev"

        # Run the dev provision script
        dev.vm.provision "shell", path: ".provision_dev.sh"

        # Set up Synced Folder between roles directory and user home directory
        dev.vm.synced_folder "../", "/home/vagrant/c2games", owner: "vagrant"
        dev.vm.provider "virtualbox" do |vb|
            vb.memory = "2048"
            vb.name = "c2games_dev"
        end
        dev.vm.provider "hyperv" do |hv|
            hv.memory = "2048"
            hv.vmname = "c2games_dev"
        end
    end

    config.vm.define "centos_test" do |centos_test|
        centos_test.vm.box = "centos/7"
        centos_test.vm.hostname = "centos-test"

        # Add ansible user. Useradd use the vagrant user as the skel directory, copying the Ansible public/private key pair
        # Default Ansible Password: ansible
        centos_test.vm.provision "shell", inline: 'grep -q ansible /etc/passwd || sudo useradd --skel /home/vagrant -m -p $(echo -n "ansible" | openssl passwd -1 -stdin) -s /bin/bash ansible'
        centos_test.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.name = "c2games_centos_test"
        end
        centos_test.vm.provider "hyperv" do |hv|
          hv.memory = "1024"
          hv.vmname = "c2games_centos_test"
        end
    end

    config.vm.define "centos_8_test" do |centos_8_test|
        centos_8_test.vm.box = "centos/stream8"
        centos_8_test.vm.hostname = "centos-8-test"

        # Add ansible user. Useradd use the vagrant user as the skel directory, copying the Ansible public/private key pair
        # Default Ansible Password: ansible
        centos_8_test.vm.provision "shell", inline: 'grep -q ansible /etc/passwd || sudo useradd --skel /home/vagrant -m -p $(echo -n "ansible" | openssl passwd -1 -stdin) -s /bin/bash ansible'
        centos_8_test.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.name = "c2games_centos_8_test"
        end
        centos_8_test.vm.provider "hyperv" do |hv|
          hv.memory = "1024"
          hv.vmname = "c2games_centos_8_test"
        end
    end

    config.vm.define "ubuntu_test" do |ubuntu_test|
        ubuntu_test.vm.box = "generic/ubuntu1804"
        ubuntu_test.vm.hostname = "ubuntu-test"

        # Add ansible user. Useradd use the vagrant user as the skel directory, copying the Ansible public/private key pair
        # Default Ansible Password: ansible
        ubuntu_test.vm.provision "shell", inline: 'grep -q ansible /etc/passwd || sudo useradd --skel /home/vagrant -m -p $(echo -n "ansible" | openssl passwd -1 -stdin) -s /bin/bash ansible'
        ubuntu_test.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.name = "c2games_ubuntu_test"
        end
        ubuntu_test.vm.provider "hyperv" do |hv|
          hv.memory = "1024"
          hv.vmname = "c2games_ubuntu_test"
        end
    end
    config.vm.define "ubuntu_20_test" do |ubuntu_20_test|
        ubuntu_20_test.vm.box = "generic/ubuntu2004"
        ubuntu_20_test.vm.hostname = "ubuntu-20-test"

        # Add ansible user. Useradd use the vagrant user as the skel directory, copying the Ansible public/private key pair
        # Default Ansible Password: ansible
        ubuntu_20_test.vm.provision "shell", inline: 'grep -q ansible /etc/passwd || sudo useradd --skel /home/vagrant -m -p $(echo -n "ansible" | openssl passwd -1 -stdin) -s /bin/bash ansible'
        ubuntu_20_test.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.name = "c2games_ubuntu_20_test"
        end
        ubuntu_20_test.vm.provider "hyperv" do |hv|
          hv.memory = "1024"
          hv.vmname = "c2games_ubuntu_20_test"
        end
    end
end
