# Vagrant Cheatsheet
--------------------

Refer to https://www.c2games.org/development-environment/ for assistance setting up a development environment.

## VM Information

* The default logins to these systems are:

Username: vagrant
Password: vagrant

Username: ansible
Password: ansible

Both have password-less sudo configured.

**NOTE: By Default, the VMs are configured with "Bridged" networking within VirtualBox.
This means that vulnerable VMs are put on the same network as the host system,
ex - your home Wifi or employer network. Use appropriate caution
when creating vulnerable systems.**

* The IP Address of each system is available on the TTY Login Screen. See Example Below:

```
 ____________________________________________________________________
|                                                                    |
| CentOS Linux 7 (Core)                                              |
| Kernel 3.10.0-1127.e17.x86_64 on an x86_64                         |
|                                                                    |
|                                                                    |
| IP Address: 192.168.10.15                                          |
|                                                                    |
| centos-test login:                                                 |
|                                                                    |
|____________________________________________________________________|
```

* The root of the infrastructure-examples archive is created as a shared volume between the
 Dev VM and the host - this means all changes are saved to the host machine and
 synced to the Development VM, leaving the Development environment disposable. The shared volume can be accessed within the guest under `/home/vagrant/c2games`

## Vagrant/Workflow Information

> NOTE: All vagrant commands need to be executed from the `setup_dev_environment` directory, where the Vagrantfile is

- To SSH to a VM, use the command `vagrant ssh NAME`, where name is `dev`, `ubuntu_test` or `centos_test`

```bash
vagrant ssh centos_test # or ubuntu_test
```

* Don't forget to revert your VMs to the clean snapshot state to re-test roles from a clean state.
   * VMs can be restored using the Virtualization Provider or using Vagrant.
     To use vagrant, run following commands on the host system:
   ```
   vagrant snapshot restore centos_test clean
   vagrant snapshot restore ubuntu_test clean
   ```

* To delete all VMs using vagrant, use the command `vagrant destroy`. The VMs can also be managed using
the Virtualization Provider (ex, the VirtualBox or HyperV interface).
