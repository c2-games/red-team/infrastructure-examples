#!/bin/bash
set -ex
#
# Grab common development tools and install ansible from dedicated PPA
#
apt update
apt install -y openssh-server git software-properties-common whois sudo wget curl tmux vim ansible-lint yamllint
apt-add-repository --yes --update ppa:ansible/ansible
apt install -y ansible
