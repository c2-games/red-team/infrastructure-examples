param (
    [ValidateSet('virtualbox','hyperv')]$Provider="virtualbox"
)

function Check-ExitCode() {
  if (!$?) {
    Write-Error "Exit code $LastExitCode while running command, exiting"
  }
}

$ErrorActionPreference = "Stop"

if(!(Get-Command vagrant)) {
    Write-Host (
        "The Command 'vagrant' was not found. Please install Vagrant by downloading from:`n" +
        "`thttps://www.vagrantup.com/downloads.html"
    )
}
if(!(Get-Command ssh-keygen)) {
    Write-Host (
        "The Command 'ssh-keygen' was not found. Please install SSH client by running" +
        "the following command from an administrative powershell:`n" +
        "`tGet-WindowsCapability -Online | ? Name -like 'OpenSSH.Client*' | Add-WindowsCapability -Online"
    )
}

if (!(Test-Path -Type Container "$PSScriptRoot/.ssh")) {
    New-Item /foo/bar/baz -ItemType Directory "$PSScriptRoot/.ssh"
}

if (!(Test-Path -Type Leaf "$PSScriptRoot/.ssh/ansible_key")) {
  ssh-keygen -f "$PSScriptRoot/.ssh/ansible_key" -b 2048 -t rsa -N ""
  Check-ExitCode
}

vagrant up --provision --parallel --provider $Provider
Check-ExitCode

foreach($vm in (vagrant status | Select-String -Pattern ".*dev|.*test")) {
    $name = ($vm -split "\s+")[0]
    vagrant snapshot save $name clean
    Check-ExitCode
}
