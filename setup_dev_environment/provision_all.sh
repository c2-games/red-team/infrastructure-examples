#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
set -ex

cd $DIR  # Ensure we're in the setup directory

if [[ ! -d $DIR/.ssh || ! -f $DIR/.ssh/ansible_key.pub ]]; then
  mkdir -p "$DIR/.ssh"
  ssh-keygen -f "$DIR/.ssh/ansible_key" -b 2048 -t rsa -N ""
fi

vagrant up --provision --parallel

for VM in $(vagrant status | grep 'dev\|test' | awk '{print $1}'); do
    # Create a snapshot of the VM and name it clean
    vagrant snapshot save "${VM}" clean
done
