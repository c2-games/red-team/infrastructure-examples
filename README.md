# C2 Games Infrastructure Challenge Examples

[[_TOC_]]

C2 Games Infrastructure Challenges are structured as Ansible roles that can be deployed to a variety
of Operating Systems (OS's). Several example challenges are included in this repository.

## Quick Start Guide

After reading the full documentation provided by C2Games, the following  should serve as a refresher
to getting a new environment set up.

### Step 1: Create an Ubuntu 18.04 VM, and install Ansible
```
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

### Step 2: Create a testing VM, and install the Ansible user (note the trailing comma!)
```
ansible-playbook -i 10.1.10.17, setup_vm.yml
```

### Step 3: Snapshot VM

Snapshot the Virtual machine in it's current state, so it can easily be restored and tested against
multiple times.

If available, taking a "Memory Snapshot" or "Running Snapshot" will restore the VM to a booted
state, speeding up testing.

### Step 4: Copy the template and Write a new role
```bash
wget https://gitlab.com/c2-games/red-team/infrastructure-examples/-/archive/master/infrastructure-examples-master.zip
unzip infrastructure-examples-master.zip
cd infrastructure-examples-master
cp -r template my_new_role
```

### Step 5: Test out your new role
```yml
# Step 5a: Edit infrastructure-examples/main.yml and add your role:
    - include_role:
        name: my_new_role
```

```bash
# Step 5b: Execute your role on the Ubuntu test machine:
ansible-playbook -i 10.1.10.17 main.yml
```

### Step 6: Restore and Repeat

Restore the VM(s) to the snapshot created in Step 3, make some modifications, and continue testing.

## Frequently Asked Questions (FAQ's)

We are always up updating our FAQ's! If your answer isn't here, check out this link:
https://gitlab.com/c2-games/red-team/infrastructure-examples#Frequently%20Asked%20Questions%20(FAQ's)

If you still can't find an answer, feel free to reach out to us!

- Email: opfor@c2games.org
- Discord: https://discord.gg/H2jwMxt

### Submission Idea

> I've got an idea for a submission, but I'm not sure if something similar
> has already been completed or if it's a good fit. What do I do?

Please contact us with your idea! We'd be happy to help hash it out, We're always excited to hear
new ideas.

You can find our contact information above.

### Gitlab Merge Request

> I've completed my new infrastructure challenge, should I create a Merge Request in Gitlab?

Nope, please contact us directly with your submission. We'd like to keep challenge submissions out
of any public repositories.

### Role Not Executed

> I wrote an awesome new role named hack_stuff, but it's not running! Why?

Make sure your role is added to `examples/main.yml` in an `include_role` directive, the name
contains no typos, and the indentation is correct. The Role name is the name of the directory
containing the folders `tasks`, `handlers`, etc.

Check that you're running `ansible-playbook main.yml` from the `examples/` directory.

A valid role definition should look like this:

```yml
# ...file snip...
  tasks:
    # The include_role directive is indented more than the `tasks` keyword
    - include_role:
        # The role name is indented more than the `include_role` directive
        name: ssh_authorized_keys
```

### No hosts matched

> I got an error stating "no hosts matched" and a bunch of warnings. What gives?

A common mistake when using the `ansible-playbook` command is providing an inventory file rather
than a list of hosts. If a single host is being provided to ansible-playbook, the host must be
followed by a trailing command:

- Good: `ansible-playbook -i 10.1.11.28, main.yml`
- Bad: `ansible-playbook -i 10.1.11.28 main.yml`

If the trailing comma is omitted, the argument is interpreted as an Inventory file:
https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

The full set of warnings and errors from an incorrect command will look like:

```bash
> ansible-playbook -i 10.1.11.28 main.yml
[WARNING]: Unable to parse /home/bdavis/autodeployment/infrastructure-examples/10.1.11.28 as an inventory source
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [all] **************************************************************************************************************************************************************************
skipping: no hosts matched
```
